//50 kHz sample rate
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

#define PWM 10
#define AVclr A2
#define AVlr A1
#define AVr A0

unsigned long int t0 = 0;

unsigned long int n_samples = 50;

bool state = HIGH;

void setup() {
  //50 kHz sample rate
  sbi(ADCSRA, ADPS2);
  cbi(ADCSRA, ADPS1);
  cbi(ADCSRA, ADPS0);

  //baud rate at 115200
  Serial.begin(115200);

  pinMode(PWM, OUTPUT);
  pinMode(AVclr, INPUT);
  pinMode(AVlr, INPUT);
  pinMode(AVr, INPUT);

  digitalWrite(PWM, not(state));

  //wait to stabilize
  delay(3000);
}

void loop() {
  //voltage values arrays
  unsigned long int t_array[n_samples];
  int vr_array[n_samples];

  int count = 0;

  t0 = micros();
  
  digitalWrite(PWM, state);

  while (count < 1) {
    for (int i = 0; i != n_samples; i++) {
      t_array[i] = (micros() - t0);
      vr_array[i] = analogRead(AVr);
    }

    //print loop
    for (int k = 0; k != n_samples; k++) {
      Serial.print(t_array[k]);
      Serial.print('\t');
      Serial.print(vr_array[k] * 5. / 1023., 5);
      Serial.print('\n');
    }

    state = not(state);
    digitalWrite(PWM, state);
    count++;
  }

while (1) {} //stay in here forever
}
